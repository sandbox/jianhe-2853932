Drupal Suppliers

Project page: https://www.drupal.org/sandbox/jianhe/2853932

Synopsis
--------
Drupal Suppliers consists of Supplier Lifecycle Management and Supplier Hub.
Supplier Lifecycle Management provides an extensive set of features to
maintain supplier information.
Supplier Hub provides a portfolio of Master Data Management tools to enable
organizations to better manage their supplier master records.

Requirements
------------
business_core
(https://www.drupal.org/sandbox/jianhe/2846751)

Dependencies
------------
Drupal 8.x

Clone command
-------------
git clone --branch 8.x-2.x jianhe@git.drupal.org:sandbox/jianhe/2853932.git \
suppliers
